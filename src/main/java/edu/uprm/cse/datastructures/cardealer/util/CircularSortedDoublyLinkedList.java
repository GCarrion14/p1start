package edu.uprm.cse.datastructures.cardealer.util;

import java.lang.reflect.Array;
import java.util.Comparator;
import java.util.Iterator;

/**
 * Circular Sorted Doubly Linked List class implementing SortedList interface.
 *
 * @param <E>
 */
public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {

	private DNode<E> header;
	private int size;
	private Comparator<E> comparator;

	/**
	 * Initializes list, sets header pointing to itself and size to 0.
	 * 
	 * @param comp - to sort list.
	 */
	public CircularSortedDoublyLinkedList(Comparator<E> comp) {
		this.header = new DNode<E>();
		this.header.setPrev(this.header);
		this.header.setNext(this.header);
		this.size = 0;
		comparator = comp;
	}
	
	public CircularSortedDoublyLinkedList() {
		this.header = new DNode<E>();
		this.header.setPrev(this.header);
		this.header.setNext(this.header);
		this.size = 0;
	}

	/**
	 * Returns the size of the list. 
	 * return size - of list
	 */
	@Override
	public int size() {
		return size;
	}

	/**
	 * Adds object to list on corresponding spot based on the sorting from the comparator.
	 * 
	 * @param obj - can't be null, generic type.
	 * 
	 * @return boolean - if object is added returns true, false otherwise.
	 */
	@Override
	public boolean add(E obj) {
		if (obj == null)
			return false;
		DNode<E> node = new DNode<E>(obj);
		DNode<E> current = header.getNext();
		while (current != header && comparator.compare(current.getElement(), node.getElement()) < 0) {
			current = current.getNext();
		}
		node.setNext(current);
		node.setPrev(current.getPrev());
		current.getPrev().setNext(node);
		current.setPrev(node);
		size++;
		return true;
	}

	/**
	 * If list is not empty, and object is in list, removes the object.
	 * 
	 * @param obj - can't be null, generic type.
	 * 
	 * @return boolean - if object is removed returns true, false otherwise.
	 */
	@Override
	public boolean remove(E obj) {
		if (isEmpty() || obj == null || !contains(obj))
			return false;
		DNode<E> current = header.getNext();
		while (current != header && !current.getElement().equals(obj)) {
			current = current.getNext();
		}
		DNode<E> prev = current.getPrev();
		prev.setNext(current.getNext());
		current.getNext().setPrev(prev);
		current.cleanLinks();
		size--;
		return true;
	}

	/**
	 * If list is not empty, and index is valid, removes object in the index given.
	 * 
	 * @param index - valid from 0 to size-1.
	 * 
	 * @return boolean - if object is removed returns true, false otherwise.
	 */
	@Override
	public boolean remove(int index) {
		if (isEmpty() || index<0 || index>=size)
			throw new IndexOutOfBoundsException();
		DNode<E> current = header.getNext();
		int i = 0;
		while (current != header && i < index) {
			current = current.getNext();
			i++;
		}
		DNode<E> prev = current.getPrev();
		prev.setNext(current.getNext());
		current.getNext().setPrev(prev);
		current.cleanLinks();
		size--;
		return true;
	}

	/**
	 * If list is not empty, while object is in list, removes all copies of object.
	 * 
	 * @param obj - can't be null.
	 * 
	 * @return count - count of objects that were removed.
	 */
	@Override
	public int removeAll(E obj) {
		int count = 0;
		while (remove(obj)) {
			count++;
		}
		return count;
	}

	/**
	 * If list is not empty, returns the first element.
	 * 
	 * @return firstElement of list.
	 */
	@Override
	public E first() {
		if (isEmpty())
			return null;
		return header.getNext().getElement();
	}

	/**
	 * If list is not empty, returns the last element.
	 * 
	 * @return lastElement of list.
	 */
	@Override
	public E last() {
		if (isEmpty())
			return null;
		return header.getPrev().getElement();
	}

	/**
	 * Gets object on index from list. If the index isn't valid, returns null.
	 * 
	 * @param index - index of object to return.
	 * 
	 * @return element - element at index.
	 */
	@Override
	public E get(int index) {
		if (index >= size || index < 0 || isEmpty())
			return null;
		DNode<E> current = header.getNext();
		for (int i = 0; i < index; i++) {
			if (current != header)
				current = current.getNext();
		}
		return current.getElement();
	}

	/**
	 * Removes all objects from list.
	 */
	@Override
	public void clear() {
		while (size > 0) {
			remove(0);
		}
	}

	/**
	 * Check if list contains the object.
	 * 
	 * @param e - object to search for in list.
	 * 
	 * @return boolean - true if found, false otherwise.
	 */
	@Override
	public boolean contains(E e) {
		DNode<E> current = header.getNext();
		for (int i = 0; i < size; i++) {
			if (current.getElement().equals(e))
				return true;
			current = current.getNext();
		}
		return false;
	}

	/**
	 * Returns if list is empty.
	 * 
	 * @return boolean - true if list is empty, false otherwise.
	 */
	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	/**
	 * Returns the position where the first instance of e is located in list.
	 * 
	 * @param e - object to find.
	 * 
	 * @return index - location of object, -1 if not found.
	 */
	@Override
	public int firstIndex(E e) {
		DNode<E> current = header.getNext();
		for (int i = 0; i < size; i++) {
			if (current.getElement().equals(e))
				return i;
			current = current.getNext();
		}
		return -1;
	}

	/**
	 * Returns the position where the last instance of e is located in list.
	 * 
	 * @param e - object to find.
	 * 
	 * @return index - location of object, -1 if not found.
	 */
	@Override
	public int lastIndex(E e) {
		DNode<E> current = header.getPrev();
		for (int i = size - 1; i >= 0; i--) {
			if (current.getElement().equals(e))
				return i;
			current = current.getPrev();
		}
		return -1;
	}

	/**
	 * Iterator for the CSDLList.
	 */
	@Override
	public Iterator<E> iterator() {
		Iterator<E> it = new Iterator<E>() {

			private int i = 0;

			@Override
			public boolean hasNext() {
				return i < size;
			}

			@Override
			public E next() {
				return get(i++);
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
		return it;
	}

	/**
	 * toString for testing purposes. Prints out the list in array form. {element0, element1, ...}
	 * ifEmpty returns {}.
	 * 
	 * @return string - represents the array form of the list.
	 */
	@Override
	public String toString() {
		if (isEmpty())
			return "{}";
		DNode<E> current = header.getNext();
		String str = "{" + current.getElement().toString();
		for (int i = 1; i < size; i++) {
			current = current.getNext();
			str += ", " + current.getElement().toString();
		}
		str += "}";
		return str;
	}

	/**
	 * toArray method of a linked list.
	 * 
	 * @return array - array containing all the elements in the linked list.
	 */
	public Object[] toArray() {
		Object[] array = new Object[size];
		DNode<E> current = header.next;
		for(int i = 0; i<size; i++) {
			array[i] = current.getElement();
			current = current.getNext();
		}
		return array;
	}
	
	/**
	 * toArrayMethod when given an array.
	 * 
	 * @param array
	 * 
	 * @return array - array with all the elements and if array size is bigger than list size, sets positions to null.
	 */
	public <E> E[] toArray(E[] array) {
		if(array.length < size) {
			array = (E[]) Array.newInstance(array.getClass().getComponentType(), size);
		}
		else if(array.length > size){
			for(int i = 0; i<size; i++) {
				array[i] = null;
			}
		}
		DNode<E> current = (DNode<E>) header.getNext();
		for(int i = 0; i<size; i++) {
			array[i] = current.getElement();
			current = current.getNext();
		}
		return array;
	}

	
	/**
	 * Class to represent a node of the type used in doubly linked lists.
	 * 
	 * @param <E>
	 */
	private static class DNode<E> implements Node<E> {
		private E element;
		private DNode<E> prev, next;

		// Constructors
		public DNode() {
		}

		public DNode(E e) {
			element = e;
		}

		public DNode(E e, DNode<E> p, DNode<E> n) {
			prev = p;
			next = n;
		}

		// Methods
		public DNode<E> getPrev() {
			return prev;
		}

		public void setPrev(DNode<E> prev) {
			this.prev = prev;
		}

		public DNode<E> getNext() {
			return next;
		}

		public void setNext(DNode<E> next) {
			this.next = next;
		}

		@Override
		public E getElement() {
			return element;
		}

		@Override
		public void setElement(E data) {
			element = data;
		}

		/**
		 * Just set references prev and next to null. Disconnect the node from the
		 * linked list....
		 */
		public void cleanLinks() {
			prev = next = null;
		}

	}
	
	private interface Node<E> {
		public E getElement();
		public void setElement(E data);
	}


}
