package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class CarList{

	private static CircularSortedDoublyLinkedList<Car> list = new CircularSortedDoublyLinkedList<Car>(new CarComparator<Car>());
	
	public static void resetCars() {
		list.clear();
		list = new CircularSortedDoublyLinkedList<Car>(new CarComparator<Car>());
	}
	
	public static CircularSortedDoublyLinkedList<Car> getInstance(){
		return list;
	}

}
