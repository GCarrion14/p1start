package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

/**
 * Comparator for Car class.
 *
 * @param obj1 Car object to be compared.
 * @param obj2 Car object to be compared.
 */
public class CarComparator<E> implements Comparator<E> {

	@Override
	public int compare(E obj1, E obj2) {
		if(obj1 instanceof Car && obj2 instanceof Car) {
			Car car1 = (Car) obj1;
			Car car2 = (Car) obj2;
			return (car1.getCarBrand() + car1.getCarModel() + car1.getCarModelOption()).compareTo(
					(car2.getCarBrand() + car2.getCarModel() + car2.getCarModelOption()));
		}
		else 
			throw new IllegalStateException("Object isnt an instance of Car.");
	}
}
